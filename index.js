const liens = document.querySelectorAll(".navbar-light .navbar-nav .nav-link");

// Quand la souris est au dessus des liens
liens.forEach((lien) => {
  lien.addEventListener("mouseenter", () => {
    lien.style.background = "blue";
    lien.style.color = "white";
  });
});
// Quand la souris n'est plus sur les liens
liens.forEach((lien) => {
  lien.addEventListener("mouseout", () => {
    lien.style.background = "black";
  });
});

// Au moment du clique sur les liens
liens.forEach((lien) => {
  lien.addEventListener("click", (e) => {
    e.target.style.background = "red";
  });
});

//--------Formulaire de contact

myForm.addEventListener("submit", (e) => {
  const name = document.getElementById("name");
  const mail = document.getElementById("mail");
  const message = document.getElementById("msg");
  const error = document.getElementById("error");
  const myRegex = /^[a-zA-Z-\s]+$/;

  // Messages d'erreurs pour les champs vides
  if (message.value.trim() == "") {
    error.innerHTML = "Veuillez renseigner un message.";
    error.style.color = "red";
    e.preventDefault();
  }

  if (mail.value.trim() == "") {
    error.innerHTML = "Veuillez renseigner un email.";
    error.style.color = "red";
    e.preventDefault();
  }

  if (name.value.trim() == "") {
    error.innerHTML = "Veuillez renseigner un nom.";
    error.style.color = "red";
    e.preventDefault();
  } else if (myRegex.test(name.value) == false) {
    error.innerHTML =
      "Le nom doit comporter des lettres, des tirets uniquements.";
    error.style.color = "red";
    e.preventDefault();
  }
  // validation du formulaire
  if ((myForm = name.value && mail.value && message.value)) {
    alert("le formulaire est envoyé");
  }
});

// ------ Quand la souris est sur la barre de pourcentage la couleur change
const pourcentages = document.querySelectorAll(".progress-bar");

pourcentages.forEach((pourcentage) => {
  pourcentage.addEventListener("mouseenter", () => {
    pourcentage.style.background = "green";
  });
});

pourcentages.forEach((pourcentage) => {
  pourcentage.addEventListener("mouseout", () => {
    pourcentage.style.background = "#92922e";
  });
});

// ------------------Texte animé
const target = document.getElementById("target");

let array = [
  "Junior !",
  "CSS !",
  "Javascript !",
  "PHP !",
  "SYMFONY !",
  "Full-Stack !",
];
let wordIndex = 0;
let letterIndex = 0;

// Fonction pour afficher les lettres
const createLetter = () => {
  const letter = document.createElement("span");
  target.appendChild(letter);

  letter.textContent = array[wordIndex][letterIndex];
  setTimeout(() => {
    letter.remove();
  }, 2800);
};
// Fonction afin d'afficher le texte animé en boucle
const loop = () => {
  setTimeout(() => {
    if (wordIndex >= array.length) {
      wordIndex = 0;
      letterIndex = 0;
      loop();
    } else if (letterIndex < array[wordIndex].length) {
      createLetter();
      letterIndex++;
      loop();
    } else {
      wordIndex++;
      letterIndex = 0;
      setTimeout(() => {
        loop();
      }, 2800);
    }
  }, 40);
};
loop();
