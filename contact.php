<?php
// le fichier contact.php est relié au fichier dbconnect.php pour la base de données
require_once("dbconnect.php");

if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message'])) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    // si les champs ne sont pas vides
    if (!empty($name) && !empty($email) && !empty($message)) {
        // On enregistre les éléments dans la table contact qui se trouve dans la base de données portfolio
        $db = dbConnect("portfolio", "root", "");
        $insert_contact = "INSERT INTO contact (name, email, message) VALUES (:name, :email, :message)";
        $stmt = $db->prepare($insert_contact);

        $stmt->execute([
            ":name" => $name,
            ":email" => $email,
            ":message" => $message
        ]);

        header('Location: index.html#accueil');
    }
}
