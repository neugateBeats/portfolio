<?php
// Connexion de la base de données

function dbConnect($dbname, $username, $password)
{
    try {
        $db = new PDO("mysql:host=localhost;dbname=" . $dbname . ";charset=utf8;", $username, $password);
    } catch (PDOException $exception) {
        die("Erreur connexion à la base de données");
    }
    return $db;
}
